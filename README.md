# Boilerplate Cypress API

## Ferramentas utilizadas:  
- [JS](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript) 
- [npm](https://www.npmjs.com/)
- [cypress](https://docs.cypress.io/)

### Pré-requisitos: 
- Instalação [**Node-16.14.2**](https://nodejs.org/en/about/releases/)
- Instalação [**NVM**](https://heynode.com/tutorial/install-nodejs-locally-nvm/)


## Installing NVM:

```bash
$ nvm install 16.14.2
$ nvm use 16.14.2
```
## Installing Project:

```bash
$ yarn 
ou 
$ yarn install
```