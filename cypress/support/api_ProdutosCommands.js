/// <reference types="Cypress" />

Cypress.Commands.add('GET_SearchProducts', (queryString) => {
	cy.request({
		method: 'GET',
		url: '/produtos?' + queryString,
	})
})

Cypress.Commands.add('GET_SearchAllProducts', () => {
	cy.request({
		method: 'GET',
		url: '/produtos',
	})
})

Cypress.Commands.add('POST_SignUpProduct', (bodyJson) => {
	cy.request({
		method: 'POST',
		url: '/produtos',
		body: bodyJson,
		headers: { Authorization: localStorage.getItem('token') },
	})
})

Cypress.Commands.add('DELETE_DeleteProduct', (productId, failStatusCode) => {
	cy.request({
		method: 'DELETE',
		url: '/produtos/' + productId,
		headers: { Authorization: localStorage.getItem('token') },
		failOnStatusCode: failStatusCode,
	})
})

Cypress.Commands.add(
	'DELETE_DeleteProductWithoutPermissions',
	(productId, failStatusCode) => {
		cy.request({
			method: 'DELETE',
			url: '/produtos/' + productId,
			headers: { Authorization: localStorage.getItem('token') },
			failOnStatusCode: failStatusCode,
		})
	}
)
